/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leonardo.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Leonardo
 */
@Entity
@Table(name = "SQUADRA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Squadra.findAll", query = "SELECT s FROM Squadra s"),
    @NamedQuery(name = "Squadra.findByNome", query = "SELECT s FROM Squadra s WHERE s.nome = :nome"),
    @NamedQuery(name = "Squadra.findByNazione", query = "SELECT s FROM Squadra s WHERE s.nazione = :nazione"),
    @NamedQuery(name = "Squadra.findByAnnofondazione", query = "SELECT s FROM Squadra s WHERE s.annofondazione = :annofondazione")})
public class Squadra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "NOME")
    private String nome;
    @Size(max = 45)
    @Column(name = "NAZIONE")
    private String nazione;
    @Column(name = "ANNOFONDAZIONE")
    private Integer annofondazione;
    @JoinColumn(name = "CAMPIONATO", referencedColumnName = "NOME")
    @ManyToOne
    private Campionato campionato;
    @OneToMany(mappedBy = "squadra")
    private Collection<Dirigente> dirigenteCollection;

    public Squadra() {
    }

    public Squadra(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNazione() {
        return nazione;
    }

    public void setNazione(String nazione) {
        this.nazione = nazione;
    }

    public Integer getAnnofondazione() {
        return annofondazione;
    }

    public void setAnnofondazione(Integer annofondazione) {
        this.annofondazione = annofondazione;
    }

    public Campionato getCampionato() {
        return campionato;
    }

    public void setCampionato(Campionato campionato) {
        this.campionato = campionato;
    }

    @XmlTransient
    public Collection<Dirigente> getDirigenteCollection() {
        return dirigenteCollection;
    }

    public void setDirigenteCollection(Collection<Dirigente> dirigenteCollection) {
        this.dirigenteCollection = dirigenteCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nome != null ? nome.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Squadra)) {
            return false;
        }
        Squadra other = (Squadra) object;
        if ((this.nome == null && other.nome != null) || (this.nome != null && !this.nome.equals(other.nome))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nome;
    }
    
}

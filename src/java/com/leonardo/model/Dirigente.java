/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leonardo.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Leonardo
 */
@Entity
@Table(name = "DIRIGENTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dirigente.findAll", query = "SELECT d FROM Dirigente d"),
    @NamedQuery(name = "Dirigente.findById", query = "SELECT d FROM Dirigente d WHERE d.id = :id"),
    @NamedQuery(name = "Dirigente.findByNome", query = "SELECT d FROM Dirigente d WHERE d.nome = :nome"),
    @NamedQuery(name = "Dirigente.findByCognome", query = "SELECT d FROM Dirigente d WHERE d.cognome = :cognome"),
    @NamedQuery(name = "Dirigente.findByNazionalita", query = "SELECT d FROM Dirigente d WHERE d.nazionalita = :nazionalita")})
public class Dirigente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 45)
    @Column(name = "NOME")
    private String nome;
    @Size(max = 45)
    @Column(name = "COGNOME")
    private String cognome;
    @Size(max = 45)
    @Column(name = "NAZIONALITA")
    private String nazionalita;
    @JoinColumn(name = "SQUADRA", referencedColumnName = "NOME")
    @ManyToOne
    private Squadra squadra;

    public Dirigente() {
    }

    public Dirigente(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getNazionalita() {
        return nazionalita;
    }

    public void setNazionalita(String nazionalita) {
        this.nazionalita = nazionalita;
    }

    public Squadra getSquadra() {
        return squadra;
    }

    public void setSquadra(Squadra squadra) {
        this.squadra = squadra;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dirigente)) {
            return false;
        }
        Dirigente other = (Dirigente) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.leonardo.model.Dirigente[ id=" + id + " ]";
    }
    
}

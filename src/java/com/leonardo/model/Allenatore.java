/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leonardo.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Leonardo
 */
@Entity
@Table(name = "ALLENATORE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Allenatore.findAll", query = "SELECT a FROM Allenatore a"),
    @NamedQuery(name = "Allenatore.findById", query = "SELECT a FROM Allenatore a WHERE a.id = :id"),
    @NamedQuery(name = "Allenatore.findByNome", query = "SELECT a FROM Allenatore a WHERE a.nome = :nome"),
    @NamedQuery(name = "Allenatore.findByCognome", query = "SELECT a FROM Allenatore a WHERE a.cognome = :cognome"),
    @NamedQuery(name = "Allenatore.findByGiocatore", query = "SELECT a FROM Allenatore a WHERE a.giocatore = :giocatore"),
    @NamedQuery(name = "Allenatore.findByNazionalita", query = "SELECT a FROM Allenatore a WHERE a.nazionalita = :nazionalita")})
public class Allenatore implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 45)
    @Column(name = "NOME")
    private String nome;
    @Size(max = 45)
    @Column(name = "COGNOME")
    private String cognome;
    @Column(name = "GIOCATORE")
    private Boolean giocatore;
    @Size(max = 45)
    @Column(name = "NAZIONALITA")
    private String nazionalita;
    @JoinColumn(name = "CAMPIONATO", referencedColumnName = "NOME")
    @ManyToOne
    private Campionato campionato;

    public Allenatore() {
    }

    public Allenatore(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public Boolean getGiocatore() {
        return giocatore;
    }

    public void setGiocatore(Boolean giocatore) {
        this.giocatore = giocatore;
    }

    public String getNazionalita() {
        return nazionalita;
    }

    public void setNazionalita(String nazionalita) {
        this.nazionalita = nazionalita;
    }

    public Campionato getCampionato() {
        return campionato;
    }

    public void setCampionato(Campionato campionato) {
        this.campionato = campionato;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Allenatore)) {
            return false;
        }
        Allenatore other = (Allenatore) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.leonardo.model.Allenatore[ id=" + id + " ]";
    }
    
}

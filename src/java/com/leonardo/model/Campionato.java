/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leonardo.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Leonardo
 */
@Entity
@Table(name = "CAMPIONATO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Campionato.findAll", query = "SELECT c FROM Campionato c"),
    @NamedQuery(name = "Campionato.findByNome", query = "SELECT c FROM Campionato c WHERE c.nome = :nome"),
    @NamedQuery(name = "Campionato.findBySerie", query = "SELECT c FROM Campionato c WHERE c.serie = :serie"),
    @NamedQuery(name = "Campionato.findByNazione", query = "SELECT c FROM Campionato c WHERE c.nazione = :nazione")})
public class Campionato implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "NOME")
    private String nome;
    @Column(name = "SERIE")
    private Integer serie;
    @Size(max = 45)
    @Column(name = "NAZIONE")
    private String nazione;
    @OneToMany(mappedBy = "campionato")
    private Collection<Squadra> squadraCollection;
    @OneToMany(mappedBy = "campionato")
    private Collection<Allenatore> allenatoreCollection;

    public Campionato() {
    }

    public Campionato(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getSerie() {
        return serie;
    }

    public void setSerie(Integer serie) {
        this.serie = serie;
    }

    public String getNazione() {
        return nazione;
    }

    public void setNazione(String nazione) {
        this.nazione = nazione;
    }

    @XmlTransient
    public Collection<Squadra> getSquadraCollection() {
        return squadraCollection;
    }

    public void setSquadraCollection(Collection<Squadra> squadraCollection) {
        this.squadraCollection = squadraCollection;
    }

    @XmlTransient
    public Collection<Allenatore> getAllenatoreCollection() {
        return allenatoreCollection;
    }

    public void setAllenatoreCollection(Collection<Allenatore> allenatoreCollection) {
        this.allenatoreCollection = allenatoreCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nome != null ? nome.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Campionato)) {
            return false;
        }
        Campionato other = (Campionato) object;
        if ((this.nome == null && other.nome != null) || (this.nome != null && !this.nome.equals(other.nome))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nome;
    }
    
}

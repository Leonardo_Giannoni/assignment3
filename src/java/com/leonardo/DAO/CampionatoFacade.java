/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leonardo.DAO;

import com.leonardo.DAO.AbstractFacade;
import com.leonardo.model.Campionato;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Leonardo
 */
@Stateless
public class CampionatoFacade extends AbstractFacade<Campionato> {

    @PersistenceContext(unitName = "CRUDwebAPPPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CampionatoFacade() {
        super(Campionato.class);
    }
    
}

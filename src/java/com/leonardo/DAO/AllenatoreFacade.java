/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leonardo.DAO;

import com.leonardo.DAO.AbstractFacade;
import com.leonardo.model.Allenatore;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Leonardo
 */
@Stateless
public class AllenatoreFacade extends AbstractFacade<Allenatore> {

    @PersistenceContext(unitName = "CRUDwebAPPPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AllenatoreFacade() {
        super(Allenatore.class);
    }
    
}

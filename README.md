Setup e Avvio applicazione
1. Scaricare e installare la versione di NetBeans 9.0.
2. Creare il database dall’IDE strutturandolo con le caratteristiche specificate nel paragrafo due del file pdf, chiamandolo footballdb con user e password entrambe root.
3. Seguire il seguente path: Service->Servers->GlassFish server.
4. Cliccare con il tasto destro sull’icona GlassFish server.
5. Cliccare la voce Start e attendere che il server si avvii.
6. Ripetere il passo 5 e selezionare la voce View Domain Admin Console.
7. In basso a sinistra nel riquadro Common Tasks della pagina web selezionare la voce JDBC Connector.
8. Cliccare JDBC Connector Pools
    Cliccare New.
    STEP 1
        Pool Name: footballdbPool
        Resource Type: javax.sql.DataSource
        Database Driver Vendor: JavaDB
    STEP 2
    Abilitare il ping
    Settare i seguenti parametri della tabella Additional Properties:
        User e Password: root
        Database name: footballdb
        ServerName: localhost
        PortNumber:1527
    Cancellare tutte le altre proprietà e cliccare finish.	
9.	 Ripetere il passo 8 e cliccare JDBC Resources.
    Cliccare New.
    Settare i seguenti parametri:
    JNDI Name: jdbc/footballdb
    Pool Name: footballdbPool
    Premere Ok in alto a destra nella pagina.
10.  Importare il progetto.

Avvio progetto tramite interfaccia grafica: Cliccare il tasto Run dell’IDE.
Avvio classi di test: seguire il path Test Packages->com.leonardo.test cliccare con il tasto destro su una delle quattro classi di test e selezionare la voce Test File.
Nota bene: i test sono stati strutturati per funzionare e dare risultato positivo su un database inizializzato con le tuple utilizzate dai tests JUnit

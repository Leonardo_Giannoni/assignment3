/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leonardo.test;

import com.leonardo.DAO.CampionatoFacade;
import com.leonardo.model.Campionato;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.NamingException;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Leonardo
 */

public class CampionatoFacadeTest {
  EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();  
    public CampionatoFacadeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
       container.close(); 
    }
    
    @Test
    public void testRemove() throws Exception {
        Campionato campionato=new Campionato();
        campionato.setNome("SerieZ");
        Campionato entity =campionato;
        CampionatoFacade instance = (CampionatoFacade)container.getContext().lookup("java:global/classes/CampionatoFacade");
        instance.remove(entity);
        
    }
    
    
    @Test
    
    public void testCreate() throws NamingException {//test metodo create Campionato
        Campionato c=new Campionato();
        c.setNome("SerieTest");
        c.setNazione("test");
        c.setSerie(1);
        Campionato entity = c;
        
        CampionatoFacade instance = (CampionatoFacade)container.getContext().lookup("java:global/classes/CampionatoFacade");
        instance.create(c);
        
    }
    
    
    /*@Test
    public void testCreateAllenatore() throws Exception {//test metodo create Allenatore
        Allenatore a=new Allenatore();
        Campionato c=new Campionato();
        c.setNome("SerieA");
        c.setNazione("ITA");
        
        a.setNome("Massimiliano");
        a.setCognome("Allegri");
        a.setNazionalita("ITA");
        a.setCampionato(c);
        a.setGiocatore(Boolean.FALSE);
        Allenatore entity = a;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        AllenatoreFacade instance = (AllenatoreFacade)container.getContext().lookup("java:global/classes/AllenatoreFacade");
        instance.create(entity);
        container.close();
    }*/
    
    
   /* @Test
    public void testCreateSqudra() throws Exception {//test metodo create Squadra
        Squadra s=new Squadra();
        s.setNome("SquadraTest");
        s.setCampionato(campionato);
        Squadra entity = null;
        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        SquadraFacade instance = (SquadraFacade)container.getContext().lookup("java:global/classes/SquadraFacade");
        instance.create(entity);
        container.close();
        
    }*/
    
    /* 
     * Test metodo edit Campionato.
    */
    
    @Test
    public void testEdit() throws Exception {
        Campionato c=new Campionato();
        c.setNome("SerieA");
        c.setNazione("ITalia");
        c.setSerie(1);
        Campionato entity = c;
       
        CampionatoFacade instance = (CampionatoFacade)container.getContext().lookup("java:global/classes/CampionatoFacade");
        instance.edit(entity);
        
    }
    
    /**
     * Test of find Campionato
     */
    
    @Test
    public void testFind() throws Exception {
        Campionato c=new Campionato();
        c.setNome("SerieA");
        Object id = c.getNome();
        //EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
        CampionatoFacade instance = (CampionatoFacade)container.getContext().lookup("java:global/classes/CampionatoFacade");
        String expResult = "SerieA";
        Campionato result = instance.find(id);
        assertEquals(expResult,result.getNome());
        //container.close();
    }

    private void assertEquals(String expResult1, String expResult) {
        if(!(expResult1.equals(expResult))){
            fail("Fail in the testFind()");
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leonardo.test;

import com.leonardo.DAO.DirigenteFacade;
import com.leonardo.model.Dirigente;
import com.leonardo.model.Squadra;

import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Leonardo
 */
public class DirigenteFacadeTest {
    EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
    public DirigenteFacadeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        container.close();
    }

    /**
     * Test of create method, of class DirigenteFacade.
     */
    @Test
    public void testCreateDirigente() throws Exception {//test metodo create Dirigente
        Dirigente d=new Dirigente();
        Squadra s=new Squadra();
        s.setNome("Milan");
        d.setNome("Mario");
        d.setCognome("Rossi");
        d.setNazionalita("Italia");
        d.setSquadra(s);
        Dirigente entity = d;
        
        DirigenteFacade instance = (DirigenteFacade)container.getContext().lookup("java:global/classes/DirigenteFacade");
        instance.create(entity);
       
    }

    /**
     * Test of edit method, of class DirigenteFacade.
     */
    @Test
    public void testEdit() throws Exception {
        Dirigente d=new Dirigente();
        Squadra s=new Squadra();
        s.setNome("Milan");
        d.setId(2);
        d.setNome("Leonardo");
        d.setCognome("Rulli");
        d.setNazionalita("Italia");
        d.setSquadra(s);
        Dirigente entity = d;
        
        DirigenteFacade instance = (DirigenteFacade)container.getContext().lookup("java:global/classes/DirigenteFacade");
        instance.edit(entity);
        
        
    }

    /**
     * Test of remove method, of class DirigenteFacade.
     */
    @Test
    public void testRemove() throws Exception {
        Dirigente d=new Dirigente();
        d.setId(11);
        Dirigente entity = d;
        DirigenteFacade instance = (DirigenteFacade)container.getContext().lookup("java:global/classes/DirigenteFacade");
        instance.remove(entity);
        
        
    }

    /**
     * Test of find method, of class DirigenteFacade.
     
     */
 @Test
    public void testFind() throws Exception {
        Dirigente d=new Dirigente();
        d.setId(2);
        Object id = d.getId();
        DirigenteFacade instance = (DirigenteFacade)container.getContext().lookup("java:global/classes/DirigenteFacade");
        int expResult = 2;
        Dirigente result = instance.find(id);
        assertEquals(expResult,result.getId());
    }

    private void assertEquals(int expResult1, int expResult) {
        if(!(expResult1==expResult)){
            fail("Fail in the testFind()");
        }
    }
}

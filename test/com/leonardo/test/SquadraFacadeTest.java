/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leonardo.test;

import com.leonardo.DAO.SquadraFacade;
import com.leonardo.model.Campionato;
import com.leonardo.model.Squadra;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Leonardo
 */
public class SquadraFacadeTest {
    EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
    public SquadraFacadeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        container.close();
    }

    /**
     * Test of create method, of class SquadraFacade.
     */
    @Test
    public void testCreate() throws Exception {
        Squadra s=new Squadra();
        Campionato c=new Campionato();
        c.setNome("SerieA");
        s.setNome("Juventus");
        s.setNazione("Italia");
        s.setAnnofondazione(1897);
        s.setCampionato(c);
        Squadra entity = s;
        
        SquadraFacade instance = (SquadraFacade)container.getContext().lookup("java:global/classes/SquadraFacade");
        instance.create(entity);
        
    }

    /**
     * Test of edit method, of class SquadraFacade.
     */
    @Test
    public void testEdit() throws Exception {
        Squadra s=new Squadra();
        Campionato c=new Campionato();
        c.setNome("SerieA");
        s.setNome("Milan");
        s.setNazione("Italia");
        s.setAnnofondazione(1988);
        s.setCampionato(c);
        Squadra entity = s;
        
        SquadraFacade instance = (SquadraFacade)container.getContext().lookup("java:global/classes/SquadraFacade");
        instance.edit(entity);
       
   
    }

    /**
     * Test of remove method, of class SquadraFacade.
     */
    @Test
    public void testRemove() throws Exception {
        Squadra s=new Squadra();
        s.setNome("SquadraElim");
        Squadra entity = s;
        
        SquadraFacade instance = (SquadraFacade)container.getContext().lookup("java:global/classes/SquadraFacade");
        instance.remove(entity);
        
    }

    /**
     * Test of find method, of class SquadraFacade.
     */
    @Test
    public void testFind() throws Exception {
        Squadra s=new Squadra();
        s.setNome("Milan");
        Object id =s.getNome();
       
        SquadraFacade instance = (SquadraFacade)container.getContext().lookup("java:global/classes/SquadraFacade");
        String expResult ="Milan";
        Squadra result = instance.find(id);
        assertEquals(expResult, result.getNome());
        
        
    }
    private void assertEquals(String expResult1, String expResult) {
        if(!(expResult1.equals(expResult))){
            fail("Fail in the testFind()");
        }
    }
}

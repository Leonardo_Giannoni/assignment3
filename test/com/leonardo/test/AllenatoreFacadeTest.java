/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leonardo.test;

import com.leonardo.DAO.AllenatoreFacade;
import com.leonardo.model.Allenatore;
import com.leonardo.model.Campionato;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Leonardo
 */
public class AllenatoreFacadeTest {
    EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
    public AllenatoreFacadeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        container.close();
    }

    /**
     * Test of create method, of class AllenatoreFacade.
     */
    @Test
    public void testCreate() throws Exception {//test metodo create Allenatore
        Allenatore a=new Allenatore();
        Campionato c=new Campionato();
        c.setNome("SerieA");
        c.setNazione("ITA");
        a.setNome("Massimiliano");
        a.setCognome("Allegri");
        a.setNazionalita("ITA");
        a.setCampionato(c);
        a.setGiocatore(Boolean.FALSE);
        Allenatore entity = a;
        AllenatoreFacade instance = (AllenatoreFacade)container.getContext().lookup("java:global/classes/AllenatoreFacade");
        instance.create(entity);
        
    }

    /**
     * Test of edit method, of class AllenatoreFacade.
     */
    @Test
    public void testEdit() throws Exception {
        Campionato c=new Campionato();
        c.setNome("SuperLeague");
        Allenatore a=new Allenatore();
        a.setId(7);
        a.setNome("Gianluca");
        a.setCognome("Zambrotta");
        a.setNazionalita("Italia");
        a.setCampionato(c);
        a.setGiocatore(Boolean.TRUE);
        Allenatore entity = a;   
        AllenatoreFacade instance = (AllenatoreFacade)container.getContext().lookup("java:global/classes/AllenatoreFacade");
        instance.edit(entity);
    }

    /**
     * Test of remove method, of class AllenatoreFacade.
     */
    @Test
    public void testRemove() throws Exception {
        Allenatore a=new Allenatore();
        a.setId(11);
        Allenatore entity = a;
        AllenatoreFacade instance = (AllenatoreFacade)container.getContext().lookup("java:global/classes/AllenatoreFacade");
        instance.remove(entity);
    }

    /**
     * Test of find method, of class AllenatoreFacade.
     */
    @Test
    public void testFind() throws Exception {
        Allenatore a=new Allenatore();
        a.setId(1);
        Object id = a.getId();
        AllenatoreFacade instance = (AllenatoreFacade)container.getContext().lookup("java:global/classes/AllenatoreFacade");
        int expResult =1;
        Allenatore result = instance.find(id);
        assertEquals(expResult, result.getId());
    }
    private void assertEquals(int expResult1, int expResult) {
        if(!(expResult1==expResult)){
            fail("Fail in the testFind()");
        }
    }
}
